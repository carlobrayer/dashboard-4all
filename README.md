# Dashboard 4all





## Available Scripts

### Passos para rodar o projeto.

Acesse o root e dentro dele faça a instalação dos pacotes necessários.

```
yarn install
```

Após a instalação correta dos pacotes, rode o projeto com o seguinte comando.

```
yarn start
```
Acesse o projeto no host indicado, geralmente [http://localhost:3000](http://localhost:3000)


Para gerar o build final.(a pasta está no gitignore)
```
yarn build
```

Notas técnicas.

1. O projeto foi desenvolvido com create react app.
2. O projeto foi ejetado.
3. O projeto roda usando SCSS dentro dos componentes, mas sem escopo fechado.
4. Foi usado local storage para persistir as mensagens no chat tendo em vista que não recebi os parâmetros do POST.
5. Foi criada uma TAG com o escopo da entrega chamada "ENTREGA", o que vier após são testes e melhorias fora de escopo para aprendizado.


Notas sobre o desenvolvedor/desenvolvimento.

1. Não tenho experiência com React, sou desenvolvedor Vue, por isso algumas coisas podem não estar no melhor padrão.
2. Os servidores da 4all estavam inativos de sexta a sábado, por isso precisei enviar somente hoje o teste.
3. Vou seguir melhorando o teste para aprender mais algumas coisas, mas considerem a tag acima citada.
4. Estou sem photoshop na máquina, por isso tamanhos de fonte, espaçamentos podem estar comprometidos.
