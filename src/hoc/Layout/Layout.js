import React, { Component } from 'react';

import './Layout.scss';

import Aux from '../Aux/Aux';

class Layout extends Component {
  state = {
    showSideDrawer: false
  }


  render () {
    return (
      <Aux className="App">
        <header className="Navegation">
          <h1 className="Navegation__Logo">
            Dashboard
          </h1>
        </header>
        <main className="Content">
            {this.props.children}
        </main>
      </Aux>
    )
  }
}

export default Layout;
