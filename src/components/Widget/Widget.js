import React from 'react';

import './Widget.scss';

const Widget = (props) => {
  return (
    <li className="Widget">
      <div className={'Widget__Symbol Widget__Symbol--' + props.bg}>
        <img alt={props.label} src={props.svg}></img>
      </div>
      <div className="Widget__Data">
        <strong>{props.data}</strong>
        <span>{props.label}</span>
      </div>
    </li>
  );
}

export default Widget;
