import React from 'react';

import './Message.scss'

import { getClassNames } from 'dynamic-class-list';


const message = React.memo((props) => (
  <article  className={getClassNames('Message', {'Message--Reversed': props.reversed})}>

      <div className="Message__Portrait">
        <img  alt={props.userName} src={props.portrait} />
      </div>

      <div className="Message__Text">
        <div className="Info">
          <span>
            <strong>{ props.userName } </strong>
            { props.time }
          </span>
          <p>{ props.message }</p>
        </div>
      </div>

  </article>
));

export default message;
