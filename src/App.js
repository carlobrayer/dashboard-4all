import React, { Component } from 'react';

import Layout from './hoc/Layout/Layout';

import './App.scss';

import Messages from './containers/Messages/Messages';
import WidgetList from './containers/WidgetList/WidgetList';
import Chart from './containers/Chart/Chart';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout>
          <WidgetList />
          <Chart />
          <Messages></Messages>
        </Layout>
      </div>
    );
  }
}

export default App;
