import React, { Component } from 'react'

import './Messages.scss'

import axios from 'axios'
import ls from 'local-storage'

import Message from '../../components/Messages/Message/Message';
import MessageForm  from './MessageForm/MessageForm'

class Messages extends Component {
  state = {
    messages: []
  }

  componentDidMount (){

    axios.get('http://dev.4all.com:3050/messages')
      .then((response) => {
        const messages =  response.data;
        const updatedMessages = messages.map((message, index) => {
          return {
              ...message,
              id: index
          }
        })
        this.setLocalStorage(updatedMessages);
      }).catch((err) => {
        console.log(err);
      });

  }

  setLocalStorage = (response) => {

    if(ls.get('dash4all')){
      this.setState({messages: ls.get('dash4all')});
    }else{
      ls.set('dash4all', response);
      this.setState({messages: ls.get('dash4all')});
    }

  }

  sendForm = (message) => {

    let updatedMessages = this.state.messages;

    updatedMessages.unshift({
      userName: 'Me',
      portrait: "messages/me.png",
      message: message,
      displayPortraitLeft: true,
      time: "1 min ago",
      id: (updatedMessages.length +1)
    });

    ls.set('dash4all', updatedMessages);
    this.setState({messages: ls.get('dash4all')});

    //Post sem referência de conteúdo.
    axios.post('/messages', {
      message: {
        userName: 'me',
        portrait: "messages/me.png",
        message: message,
        displayPortraitLeft: true,
        time: "1 min ago",
        id: (updatedMessages.length +1)
      }
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (err) {
      console.log(err);
    });

  }



  render() {
    const messages = this.state.messages.map(
      message => {
        return <Message
                  key={message.id}
                  userName={message.userName}
                  message={message.message}
                  portrait={message.portrait}
                  reversed={message.displayPortraitLeft}
                  time={message.time}/>
      }
    );
    return (
      <div className="Messages">
        <h2 className="Messages__Title">Chat</h2>
        <div className="Messages__Container">
          { messages }
          <MessageForm
            sendForm={this.sendForm} />
        </div>
      </div>
    )
  }
}

export default Messages;
