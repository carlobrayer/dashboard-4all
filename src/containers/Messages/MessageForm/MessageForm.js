import React, { Component } from 'react';

import './MessageForm.scss'

class MessageForm extends Component {

  constructor(props) {
    super(props);

    this.sendForm = props.sendForm;

    this.state = {
      message: '',
      error: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({message: event.target.value});
  }

  handleSubmit(event) {

    event.preventDefault();

    if(this.state.message.length > 0){
      this.sendForm(this.state.message);
      this.setState({message: ''});
    }else{
      console.log('false');
      //AddClass
    }

  }

  render() {
    return (
      <form className="MessageForm" onSubmit={this.handleSubmit}>
        <div className="MessageForm__Control">
          <input className="MessageForm__Control__Input" placeholder="Type your message here..." type="text" value={this.state.message} onChange={this.handleChange} />
          <button className="MessageForm__Control__Button">Send</button>
        </div>
      </form>
    );
  }

}

export default MessageForm;
