import React, { Component } from 'react'

import './Chart.scss'

import axios from 'axios'
import {Line} from 'react-chartjs-2'


//Original Conf
var data = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
        {
          fill: true,
          lineTension: 0.5,
          backgroundColor: 'rgba(25, 181, 254, 0.6)',
          borderColor: 'rgba(25, 181, 254, 1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'round',
          pointBorderColor: 'rgba(25, 181, 254, 1)',
          pointBackgroundColor: 'rgba(25, 181, 254, 1)',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(25, 181, 254, 0.6)',
          pointHoverBorderColor: 'rgba(25, 181, 254, 1)',
          pointHoverBorderWidth: 2,
          pointRadius: 7,
          pointHitRadius: 10,
          hidden: false,
          data: [0, 0, 0, 0, 0, 0, 0]
        }
      ],
      options: {
        responsive: true,
        legend: {
          position: 'left',
        }
      }
    };


class Chart extends Component {

  componentDidMount (){


    axios.get('pageViews')
      .then((response) => {

        let labels = [];
        let views = [];

        Object.entries(response.data).forEach(
          ([key, value], index) => {
            labels.push(value.month)
            views.push(value.views)
          }
        )

        data.labels = labels;
        data.datasets[0].data = views;

        this.setState({chartData: data});

      }).catch((err) => {
        console.log('error');
      });
  }

  state = {
    chartData: data
  }

  render() {
    return (
      <div className="Chart">
        <Line
          ref={(reference) => this.chartReference = reference }
          data={this.state.chartData}
          options={{
            legend: false,
            layout: {
              padding: {
                  left: 5,
                  right: 0,
                  top: 0,
                  bottom: 0
              }
            }
          }}
          redraw
          />
      </div>
    )
  }
}



export default Chart;
