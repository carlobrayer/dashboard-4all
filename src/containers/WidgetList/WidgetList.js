import React, { Component } from 'react'

import './WidgetList.scss';

import axios from 'axios'

import Widget from '../../components/Widget/Widget';




const WIDGET_DATA = [
  {
    bg: 'blue',
    svgPath: 'widgets/trash.svg'
  },
  {
    bg: 'yellow',
    svgPath: 'widgets/chat.svg'
  },
  {
    bg: 'green',
    svgPath: 'widgets/avatar.svg'
  },
  {
    bg: 'red',
    svgPath: 'widgets/pageview.svg'
  }
]


export default class WidgetList extends Component {

  state = {
    widgets: []
  }

  componentDidMount (){

    axios.get('widgets')
      .then((response) => {
        var data = response.data;
        const updatedWidgets = [];

        Object.entries(data).forEach(
          ([key, value], index) => {
            updatedWidgets.push({
              data: value,
              label: key,
              id: index,
              bg: WIDGET_DATA[index].bg,
              svg: WIDGET_DATA[index].svgPath
            });
          }
        )
        this.setState({widgets: updatedWidgets});


      }).catch((err) => {
        console.log('error');
      });
  }

  render() {

    const widgets = this.state.widgets.map(
      widget => {
        return <Widget
                data={widget.data}
                label={widget.label}
                bg={widget.bg}
                svg={widget.svg}
                key={widget.id} />
      }
    );
    return (
      <ul className="WidgetList">
        {widgets}
      </ul>
    )
  }
}
